# syntax=docker/dockerfile:1

FROM node:20-alpine

ENV NEXT_TELEMETRY_DISABLED=1

RUN set -eux; \
	apk add --no-cache su-exec sudo; \
	addgroup node wheel; \
	echo "%wheel ALL=(ALL:ALL) NOPASSWD: ALL" > /etc/sudoers.d/tmp; \
	mkdir /home/node/project

WORKDIR /home/node/project

COPY                   ./docker-entrypoint.sh /usr/local/bin
COPY --chown=node:node ./package.json .
COPY --chown=node:node ./.next        ./.next
COPY --chown=node:node ./node_modules ./node_modules

ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]

CMD ["nextjs"]
