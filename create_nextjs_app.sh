#!/bin/sh

if [ -z "$(which jq)" ];then
	echo ERR: cannot find 'jq' util, exit
	exit
fi

export RRRR=./
#mkdir $RRRR || exit
#cd $RRRR
npm install next@latest react@latest react-dom@latest

export RRRR='"scripts":{"dev":"next dev","build":"next build","start":"next start","lint":"next lint"},'
cat ./package.json | sed "1 s/.*/{$RRRR/" | jq > package.json~ && mv package.json~ package.json

mkdir app

cat <<EOF > app/layout.js
export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body>{children}</body>
    </html>
  )
}
EOF


cat <<EOF > app/page.js
export default function Page() {
  return <h1>Hello, 111!</h1>
}
EOF


cat <<EOF > .gitignore
# See https://help.github.com/articles/ignoring-files/ for more about ignoring files.

# dependencies
/node_modules
/.pnp
.pnp.js
.yarn/install-state.gz

# testing
/coverage

# next.js
/.next/
/out/

# production
/build

# misc
.DS_Store
*.pem

# debug
npm-debug.log*
yarn-debug.log*
yarn-error.log*

# local env files
.env*.local

# vercel
.vercel

# typescript
*.tsbuildinfo
next-env.d.ts
EOF


echo DONE!!!
