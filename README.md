## test_nextjs_CICD
Test project

## Description
CI/CD testing

## Zero downtime testing
```sh
docker run --rm --network host --name ab11 httpd bash -c 'ab -s600 -t60 -n10000000 -c1 http://127.0.0.1/' | grep 'Failed requests'
```
