#!/bin/sh

NODE_FICTIVE_UID=1000
NODE_FICTIVE_USER=node
if [ -z "$NODE_UID" ];then
	NODE_UID=$NODE_FICTIVE_UID
fi

set -eu

if [ $NODE_FICTIVE_UID != $NODE_UID ];then
	if [[ $(id -u) = 0 ]];then
		chown -R $NODE_UID:$NODE_UID $(getent passwd $NODE_FICTIVE_USER | awk -F: '{print $6}')
		
		sed -i "s/:${NODE_FICTIVE_UID}:/:${NODE_UID}:/"                                 /etc/group
		sed -i "s/:${NODE_FICTIVE_UID}:${NODE_FICTIVE_UID}:/:${NODE_UID}:${NODE_UID}:/" /etc/passwd && delgroup $NODE_FICTIVE_USER wheel
	else
		sudo sh -c "chown -R $NODE_UID:$NODE_UID $(getent passwd $NODE_FICTIVE_USER | awk -F: '{print $6}')"
		
		sudo sh -c "sed -i \"s/:${NODE_FICTIVE_UID}:/:${NODE_UID}:/\"                                 /etc/group"
		sudo sh -c "sed -i \"s/:${NODE_FICTIVE_UID}:${NODE_FICTIVE_UID}:/:${NODE_UID}:${NODE_UID}:/\" /etc/passwd && delgroup $NODE_FICTIVE_USER wheel"
	fi
fi

if [ "$1" = 'tail' ]; then
	exec tail -f /dev/null
fi

if [ "$1" = 'nextjs' ]; then
	exec su-exec $NODE_FICTIVE_USER yarn start -p $NEXTJS_APP_PORT --keepAliveTimeout $NEXTJS_APP_KEEPALIVETIMEOUT
fi

exec "$@"
