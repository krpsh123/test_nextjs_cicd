#!/bin/bash

set -euo pipefail


function usage {
	echo "OPTIONS"
	echo "    -h    print help and exit"
	echo "    -D    type of deployment (available values: develop or production)"
	exit
}

function container_down {
	echo "[INFO] stopping the container $1"
	docker stop $1
	
	echo "[INFO] deleting a container $1"
	docker rm $1
}

function send_haproxy {
	local action=$1
	local port=$2
	local sock=/run/haproxy-master.sock
	
	local ha_back_name='back_ff_dev'
	if [ "$O_D" = 'production' ];then ha_back_name='back_ff_prod' ;fi
	
	# @1 - это worker haproxy (http://docs.haproxy.org/2.8/management.html#9.4)
	# рабочий worker всегда будет иметь @1, даже после reload
	# тестировал так:
	#   оставил висящий tcp коннект
	#   сделал reload, и haproxy форкнул новый worker и поместил его первым,
	#   а висящий worker c tcp коннектом стал вторым, причем к висящему worker нельзя обратиться через @2
	if [ "$action" = 'enable_server' ];then
		echo "@1 enable  server ${ha_back_name}/app${port}" | socat - $sock
	else
		echo "@1 disable server ${ha_back_name}/app${port}" | socat - $sock
	fi
	
	return $?
}


if [[ -z "$@" ]];then
	usage
fi

# все завязано на названии контейнера (возможно нужно переделать на labels)
while getopts "hD:" _OPT; do
	case $_OPT in
		h)
			usage
			;;
		D)
			if [ "$OPTARG" = "develop" ];then
				container_basename=${NEXTJS_APP_IMAGE_NAME}'-dev';
				nextjs_app_available_ports=(4001 4002)
				O_D=$OPTARG
			elif [ "$OPTARG" = "production" ];then
				container_basename=${NEXTJS_APP_IMAGE_NAME}'-prod';
				nextjs_app_available_ports=(3001 3002)
				O_D=$OPTARG
			else
				echo "[ERR] invalid option 'D'" 1>&2
				usage
			fi
			;;
		*)
			usage
			;;
		esac
done
shift $((OPTIND-1))
#for item in ${nextjs_app_available_ports[*]};do echo $item ;done


###
### найдем свободный port для nextjs
###
echo "[INFO] search avaliable port to container"

busy_ports=()
for i in $(docker ps --format "table {{.Names}}");do
	if [ -z "$(grep ^$container_basename <<< $i)" ];then continue ;fi
	
	_arr=()
	for y in $(sed 's/-/ /g' <<< $i);do _arr+=($y) ;done
	
	# пропустим контейнеры на других портах
	for item in ${nextjs_app_available_ports[*]};do
		if [ $item = ${_arr[-1]} ];then
			busy_ports+=(${_arr[-1]})
		fi
	done
done
#for item in ${busy_ports[*]};do echo $item ;done

if [ ${#nextjs_app_available_ports[*]} -eq ${#busy_ports[*]} ];then
	# возможно тут надо убить старый контейнер
	# посмотреть сортировку выводимых контейнеров: docker ps
	echo "[ERR] all ports are busy" 1>&2
	exit 2
fi

nextjs_app_port=${nextjs_app_available_ports[0]}
for port in ${nextjs_app_available_ports[*]};do
	for port_busy in ${busy_ports[*]};do
		if [ $port != $port_busy ];then
			nextjs_app_port=$port
			break 2
		fi
	done
done
echo "[INFO] assigned NEXTJS_APP_PORT=$nextjs_app_port"


###
### поднимим и проверим контейнер
###
# имя нового контейнера - все остальные контейнеры с похожими именами считаются старыми версиями
container_newname=${container_basename}'-'${CI_COMMIT_SHORT_SHA}'-'${nextjs_app_port}
echo "[INFO] launching the container: $container_newname"

docker run -d --network host --restart unless-stopped \
	-v $(file $HOME/ff-nextjs/env.local-${CI_COMMIT_BRANCH} | awk '{print $5}'):/home/node/project/.env.local:ro \
	-v /etc/localtime:/etc/localtime:ro \
	-e NEXTJS_APP_PORT=$nextjs_app_port \
	-e NEXTJS_APP_KEEPALIVETIMEOUT=${NEXTJS_APP_KEEPALIVETIMEOUT} \
	--name $container_newname \
	${NEXTJS_APP_IMAGE_NAME}:${CI_COMMIT_SHORT_SHA}

nextjs_app_check_attempt=0
nextjs_app_is_running=false
while [ $nextjs_app_check_attempt -lt 7 ];do
	nextjs_app_check_attempt=$(( $nextjs_app_check_attempt + 1 ))
	echo "[INFO] Waiting start nextjs app (attempt: $nextjs_app_check_attempt)..."
	if docker exec $container_newname sh -c "wget -O /dev/null -S 127.0.0.1:${nextjs_app_port}" ;then
		nextjs_app_is_running=true
		break
	fi
	sleep 1
done
if ! $nextjs_app_is_running ;then
	container_down $container_newname
	exit 2
fi


###
### включим сервер на haproxy
###
echo "[INFO] enable server to haproxy backend"
if ! send_haproxy 'enable_server' $nextjs_app_port ;then
	container_down $container_newname
	exit 2
fi


###
### остановим старые контейнеры
###
# сейчас это сделано по номерам портов в названии контейнеров
# возможно нужно сделать так: поскольку в названии контейнера фигурирует CI_COMMIT_SHORT_SHA
# то можно остановить все контейнеры, в названии которых нет посдеднего коммита (git log --pretty=format:"%h" | head -n 10)
echo "[INFO] deleting all the old containers"

for i in $(docker ps --format "table {{.Names}}");do
	if [ -z "$(grep ^$container_basename <<< $i)" ];then continue ;fi
	
	# пропустим текущий контейнер
	if [ "$container_newname" = "$i" ];then continue ;fi
	
	container_down $i
	
	_arr=()
	for y in $(sed 's/-/ /g' <<< $i);do _arr+=($y) ;done
	
	echo "[INFO] disable server to haproxy backend"
	send_haproxy 'disable_server' ${_arr[-1]}
done
