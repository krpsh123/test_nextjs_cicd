#!/bin/bash

set -euo pipefail

### CI/CD variables
export DEPLOY=$1
# git log --pretty=format:"%h" | head -n 1
export CI_COMMIT_SHORT_SHA=$2
#
export CI_COMMIT_BRANCH=$3
#
export NEXTJS_APP_KEEPALIVETIMEOUT=35000
export NEXTJS_APP_IMAGE_NAME=ff-nextjs
export DOCK_CONT_NAME_FOR_BUILD=ff-nextjs-build


if [ "gitlab-runner" != $(id -u -n) ];then
	echo "[ERR]: launch only from the 'gitlab-runner' user, exit"
	exit
fi


# так делает gitlab-runner
rm -rf ./.next ./node_modules

echo ------------------------------------------------------------
echo ------------------ stage: build ----------------------------
echo ------------------------------------------------------------
if [ -n "$(docker ps --format '{{.Names}}' | grep -P "^${DOCK_CONT_NAME_FOR_BUILD}$")" ];then docker stop $DOCK_CONT_NAME_FOR_BUILD; sleep 3 ;fi
cat ${HOME}/ff-nextjs/env.local-${CI_COMMIT_BRANCH} > ./.env.local
docker run      -v ./:/home/node/project -v ${HOME}/home/ub:/home/ub -d --rm --init --network host --name $DOCK_CONT_NAME_FOR_BUILD node:20-alpine tail -f /dev/null
docker exec --workdir /home/node/project $DOCK_CONT_NAME_FOR_BUILD addgroup -g $(id -u gitlab-runner) ub
docker exec --workdir /home/node/project $DOCK_CONT_NAME_FOR_BUILD adduser  -u $(id -u gitlab-runner) -G ub -H -D ub
docker exec --workdir /home/node/project $DOCK_CONT_NAME_FOR_BUILD su ub -c 'yarn install --frozen-lockfile'
docker exec --workdir /home/node/project $DOCK_CONT_NAME_FOR_BUILD su ub -c 'yarn build'
rm -f ./.env.local
docker stop $DOCK_CONT_NAME_FOR_BUILD
docker build --network host -t ${NEXTJS_APP_IMAGE_NAME}:${CI_COMMIT_SHORT_SHA} .


echo ------------------------------------------------------------
echo ------------------ stage: deploy ---------------------------
echo ------------------------------------------------------------
./deploy.sh -D $DEPLOY
